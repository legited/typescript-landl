import { User } from "./User";
import { CRMType } from "./interfaces";

function main() {
  const user: User = new User('Pat', 'Eidt', CRMType.hubspot);
  const anotherUser: User = new User('Vlad', 'Romanov', CRMType.salesforce);

  console.log('Refactored CRM Services');
  console.log(user.getCrmProfile());
  console.log(anotherUser.getCrmProfile());
}

main();
