import { getCRMService } from "./CRMServiceFactory";
import { CRMType, CRMProfile } from "./interfaces";

export class User {
  private readonly firstName: string;
  private readonly lastName: string;
  private readonly crmType: CRMType;

  constructor(
    firstName: string,
    lastName: string,
    crmType: CRMType
  ) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.crmType = crmType;
  }

  public getCrmProfile(): CRMProfile {
    const crmService = getCRMService(this.crmType);
    return crmService.getProfile();
  }

}
