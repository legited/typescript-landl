import { CRMService, CRMProfile } from "./interfaces";

export class SalesforceCRMService implements CRMService {
  getProfile(): CRMProfile {
    // INSERT SALESFORCE SPECIFIC CODE / API CALLS HERE
    return {
      accountBalance: 120,
      source: 'salesforce'
    }
  }
}
