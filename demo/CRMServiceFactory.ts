import {
  CRMType,
  CRMService
} from "./interfaces";

import { HubspotCRMService } from "./HubspotCRMService";
import { SalesforceCRMService } from "./SalesforceCRMService";

export function getCRMService(crmType: CRMType): CRMService {
  switch (crmType) {
    case CRMType.hubspot:
      return new HubspotCRMService;
    case CRMType.salesforce:
      return new SalesforceCRMService;
    default:
      throw new Error ('CRM Service not available');
  }
}
