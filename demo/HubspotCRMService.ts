import { CRMService, CRMProfile } from "./interfaces";

export class HubspotCRMService implements CRMService {
  getProfile(): CRMProfile {
    // INSERT HUBSPOT SPECIFIC CODE / API CALLS HERE
    return {
      accountBalance: 100,
      source: 'hubspot'
    }
  }

}