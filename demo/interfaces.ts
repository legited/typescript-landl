export interface CRMService {
  getProfile(): CRMProfile;
}

export interface CRMProfile {
  source: string;
  accountBalance: number;
}

export enum CRMType {
  hubspot = 'hubspot',
  salesforce = 'salesforce'
}
