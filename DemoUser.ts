class DemoUser {
  private readonly firstName: string;
  private readonly lastName: string;
  private readonly address: Address;

  constructor(
    firstName: string,
    lastName: string,
    address: Address
  ) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.address = address;
  }

  public getFullName(): string {
    return this.firstName + ' ' + this.lastName;
  }

  public getAddress(): Address {
    return this.address;
  }
}

interface Address {
  streetNumber: number;
  streetName: string;
  suiteNumber?: number;
}