const UnrefactoredUser = require('./UnrefactoredUser');

function unrefactoredMain() {
  const user = new UnrefactoredUser('Pat', 'Eidt', 'hubspot');
  const anotherUser = new UnrefactoredUser('Vlad', 'Romanov', 'salesforce');

  console.log('Old CRM Profiles');
  console.log(user.getCrmProfile());
  console.log(anotherUser.getCrmProfile());
}

unrefactoredMain();