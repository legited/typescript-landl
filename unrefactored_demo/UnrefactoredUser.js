class UnrefactoredUser {

  constructor(
    firstName,
    lastName,
    crmType
  ) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.crmType = crmType;
  }

  getCrmProfile() {
    switch(this.crmType) {
      case 'hubspot':
        return {
          source: 'hubspot',
          accountBalance: 100
        };
      case 'salesforce':
        return {
          source: 'salesforce',
          accountBalance: 150
        };
    }
  }
}

module.exports = UnrefactoredUser;